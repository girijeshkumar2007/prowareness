//
//  WorkFlowListViewC.swift
//  Prowareness
//
//  Created by Girijesh Kumar on 25/02/17.
//  Copyright © 2017 Girijesh Kumar. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

let kCreateWorkFlowSegue = "CreateWorkFlowSegue"
let kCellIdentifier = "workFlowCell"
class WorkFlowListViewC: UIViewController {

    var arrOfWorkFlow = [[String:Any]]()
    @IBOutlet weak var workFlowTbView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Workflow"
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false;
        self.getAllWorkFlowAPICall()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Methods
    private func setupView(){
       // self.setUpBoaderOnView(loginBtn)
        
        workFlowTbView.tableFooterView =  UIView(frame: CGRect.zero)
        let customBtn = UIButton(type: .contactAdd)
        customBtn.tintColor = kDefaultColor;
        customBtn.addTarget(self, action: #selector(tapToAdd(_:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: customBtn)
        self.navigationItem.leftBarButtonItem = barButton;
    }
    func showConfirmationAlertWithMsg(_ identifier: String,name: String) -> Void {
        
        let alertController = UIAlertController(title: kAppName, message: "Are you sure you want to delete \(name) workflow?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { [weak self]action in
            // ...
            guard let strongRef = self else {return}
            strongRef.deleteWorkFlowAPICall(identifier)
        }
        alertController.addAction(yesAction)
        let NoAction = UIAlertAction(title: "NO", style: .cancel) { action in
            
        }
        alertController.addAction(NoAction)
        self.present(alertController, animated: true) {
            
        }
    }
    //MARK: - User Interaction
    func tapToAdd(_ sender: UIButton){
        
        self.performSegue(withIdentifier: kCreateWorkFlowSegue, sender: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let workflowDic = sender {
            
            let destViewC = segue.destination as! CreateWorkFlowViewC
            destViewC.workflowDic = workflowDic;
        }
    }
 
    
    //MARK: - API
    func getAllWorkFlowAPICall() -> Void {
        
        KRProgressHUD.show(progressHUDStyle: .black, maskType: .black, activityIndicatorStyle: .white, font: nil, message: kLoading, image: nil, completion: nil)

        NetworkManager.sharedInstance.requestApi(serviceName: kAPIAllWorkFlows, method: .get, postData: [:], encoding: URLEncoding.default) { [weak self] (response, error) in
            
            defer{
                
                KRProgressHUD.dismiss()
  
            }
            guard let strongRef = self,
            let reponseList = response as? [[String:Any]] else {return}
            strongRef.arrOfWorkFlow.removeAll()
            strongRef.arrOfWorkFlow.append(contentsOf: reponseList)
            strongRef.workFlowTbView.reloadData();
        }
    }
    
    func deleteWorkFlowAPICall(_ workflowId: String) -> Void {
        
        KRProgressHUD.show(progressHUDStyle: .black, maskType: .black, activityIndicatorStyle: .white, font: nil, message: kLoading, image: nil, completion: nil)

        let serviceName = String(format: kAPIDeleteWorkFlow, workflowId)
        NetworkManager.sharedInstance.requestApi(serviceName: serviceName, method: .delete, postData: [:], encoding: URLEncoding.default) { [weak self] (response, error) in
            
            defer{
                KRProgressHUD.dismiss()
 
                
            }
            guard let strongRef = self,
                let _ = response as? [[String:Any]] else {return}
            
           if let index = strongRef.arrOfWorkFlow.index(where: { (dic) -> Bool in
                
                guard let identifier = dic[kIdentifier] as? String, identifier == workflowId  else{return false}
                return true}){
            
                strongRef.arrOfWorkFlow.remove(at: index)
            }
            strongRef.workFlowTbView.reloadData();
        }
    }
}

extension WorkFlowListViewC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrOfWorkFlow.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.tableView(tableView, heightForRowAt: indexPath)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier)
        let nameLbl = cell?.contentView.viewWithTag(11) as! UILabel
        let stageLbl = cell?.contentView.viewWithTag(12) as! UILabel
        let dataDic = arrOfWorkFlow[indexPath.row]
        if let name = dataDic[kName] as? String
        {
            nameLbl.text = name;
        }
        if let stage = dataDic[kStage] as? String
        {
            stageLbl.text = stage;
        }
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     
        switch editingStyle {
        case .delete:
            let dataDic = arrOfWorkFlow[indexPath.row]
            if let name = dataDic[kName] as? String,
                let identifier = dataDic[kIdentifier] as? String{
                
                self.showConfirmationAlertWithMsg(identifier, name: name)
            }
            break;
        default:
            break;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dataDic = arrOfWorkFlow[indexPath.row]
        self.performSegue(withIdentifier: kCreateWorkFlowSegue, sender: dataDic)
    }
}

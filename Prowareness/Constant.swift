//
//  Constant.swift
//  Prowareness
//
//  Created by Girijesh Kumar on 25/02/17.
//  Copyright © 2017 Girijesh Kumar. All rights reserved.
//

import Foundation
import UIKit

let kName = "name"
let kUsername = "username"
let kEmail = "email"
let kPlainPassword = "plainPassword"

let kGrant_type = "grant_type"
let kClient_id = "client_id"
let kClient_secret = "client_secret"
let kPassword = "password"

let kStage = "stage"
let kAccess_token = "access_token"
let kError_description = "error_description"

let kIdentifier = "identifier"

let kLoading = "loading..."


let kDefaultColor = UIColor(red: 0.0/255.0, green: 186.0/255.0, blue: 183.0/255.0, alpha: 1.0)

//
//  BaseViewC.swift
//  Prowareness
//
//  Created by Girijesh Kumar on 25/02/17.
//  Copyright © 2017 Girijesh Kumar. All rights reserved.
//

import UIKit

class BaseViewC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpBoaderOnView(_ view: UIView) -> Void {
        
        view.layer.borderColor = UIColor.clear.cgColor;
        view.layer.masksToBounds = true;
        view.layer.cornerRadius = 5.0;
    }
    
    func showAlertWithMsg(_ msg: String, block:@escaping ((Int)->())) -> Void {
        
        let alertController = UIAlertController(title: kAppName, message: msg, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .cancel) { action in
            // ...
            block(1)
        }
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true) {
            // ...
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  LoginViewC.swift
//  Prowareness
//
//  Created by Girijesh Kumar on 25/02/17.
//  Copyright © 2017 Girijesh Kumar. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

let kRegisterSegue = "RegisterSegue"
let kWorkFlowSegue = "WorkFlowSegue"

class LoginViewC: BaseViewC {

    @IBOutlet weak var userTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    
    @IBOutlet weak var loginBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Methods
    private func setupView(){
        self.setUpBoaderOnView(loginBtn)
  
    }
    
    //MARK: - User Interaction
    @IBAction func tapToRegister(_ sender: UIButton){
        self.view.endEditing(true)
        self.performSegue(withIdentifier: kRegisterSegue, sender: nil)
    }
    @IBAction func tapToLogin(_ sender: UIButton){
        
        guard let username = userTxtField.text,
        let password = passwordTxtField.text else {
            return;
        }
        
        if username.characters.count == 0 {
            self.showAlertWithMsg("Please enter username",block: { (tag) in
            })
        }
        else if password.characters.count == 0{
            
            self.showAlertWithMsg("Please enter password",block: { (tag) in
            })
        }
        else{
            
            self.loginAPICall(userName: username, password: password)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

//    Body
//    {
//    "grant_type":"password",
//    "client_id":"{SERVER_INFO.client_id}",
//    "client_secret":"{SERVER_INFO.client_secret}",
//    "username":"{username}",
//    "password":"{password}"
//    }

    
    //MARK: - API
    func loginAPICall(userName: String, password: String) -> Void {
        
        
        KRProgressHUD.show(progressHUDStyle: .black, maskType: .black, activityIndicatorStyle: .white, font: nil, message: kLoading, image: nil, completion: nil)
        let param = [kGrant_type:kPassword,kClient_id:kClientId,kClient_secret:kClientSecret,kUsername:userName,kPassword:password]
        NetworkManager.sharedInstance.requestApi(serviceName: kAPILogin, method: .post, postData: param,encoding: JSONEncoding.default) { [weak self] (response, error) in
            
            defer{
                KRProgressHUD.dismiss()
            }
            guard let strongRef = self,
                let reponseData = response as? [String : Any] else {return}
            
            if let accessToken = reponseData[kAccess_token] as? String{
                
                NetworkManager.sharedInstance.accessToken = accessToken;
                
                strongRef.performSegue(withIdentifier: kWorkFlowSegue, sender: nil)
            }
            else if let errorMsg = reponseData[kError_description] as? String{
                
                strongRef.showAlertWithMsg(errorMsg,block: { (tag) in
                    
                })
            }
        }
    }
}

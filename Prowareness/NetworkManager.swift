//
//  NetworkManager.swift
//  Prowareness
//
//  Created by Girijesh Kumar on 25/02/17.
//  Copyright © 2017 Girijesh Kumar. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager: NSObject {

    internal static let sharedInstance: NetworkManager = {
        
        return NetworkManager()
    }()
    
    var accessToken = ""
    

//MARK:- Public Method
/**
 *  Initiates HTTPS or HTTP request over |kHTTPMethod| method and returns call back in success and failure block.
 *
 *  @param serviceName  name of the service
 *  @param method       method type like Get and Post
 *  @param postData     parameters
 *  @param responeBlock call back in block
 */
    internal func requestApi(serviceName: String, method: HTTPMethod,postData: Parameters,encoding: ParameterEncoding, completionClosure: @escaping (_ result : Any?, _ error : Error?) -> ()) -> Void{
        
        let serviceUrl = kBASEURL + serviceName
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: postData, options: JSONSerialization.WritingOptions.prettyPrinted)
            let theJSONText = NSString(data: jsonData,
                                       encoding: String.Encoding.ascii.rawValue)
            print("JSON Param String = \(theJSONText!)")
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            print(error)
        }
        print("Connecting to Host with URL \(kBASEURL)\(serviceName) jsonPara String: \(postData)");
        
        // Add Header authentication ...........
        let headers:[String:String] = getHeaderWithAPIName(serviceName: serviceName)
        print("headers \(headers)");
        Alamofire.request(serviceUrl, method: method, parameters: postData,encoding:encoding, headers: headers).responseJSON { (Response) in
            
            switch Response.result{
                
                case .success(let json):
                    print(json)
                    completionClosure(json , nil)
                    break
                case .failure(let error):
                    
                    print(error.localizedDescription)
                    completionClosure(nil , error)
                    break;
            }
        }
    }

    //MARK:- Private Method
    private func getHeaderWithAPIName(serviceName: String)-> [String:String]
    {
        // Add authentication ...........
        if serviceName == kAPIRegister || serviceName == kAPILogin {
            
            let headers:[String:String] = [kContentType:kContentValue]
            return headers;
        }
        else{
            
            let authToken = "Bearer \(accessToken)"
            let headers:[String:String] = [kAuthentication:authToken,kContentType:kContentValue]
            return headers;
        }
    }
}

//
//  RegisterViewC.swift
//  Prowareness
//
//  Created by Girijesh Kumar on 25/02/17.
//  Copyright © 2017 Girijesh Kumar. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class RegisterViewC: BaseViewC {

    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var userTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!

    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - Private Methods
    private func setupView(){
      
        self.setUpBoaderOnView(registerBtn)
        self.setUpBoaderOnView(backBtn)

    }
    func isValidEmail(candidate: String) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
        if valid {
            valid = !candidate.contains("..")
        }
        print(valid)
        return valid
    }

    //MARK: - User Interaction
    @IBAction func tapToRegister(_ sender: UIButton){
    
        guard let name = nameTxtField.text,
            let username = userTxtField.text,
            let email = emailTxtField.text,
            let password = passwordTxtField.text else {
                return;
        }
        
        if name.characters.count == 0 {
            
            self.showAlertWithMsg("Please enter your name",block: { (tag) in
            })
        }
        else if username.characters.count == 0{
            
            self.showAlertWithMsg("Please enter username",block: { (tag) in
            })
        }
        else if username.characters.count < 6{
            
            self.showAlertWithMsg("Username should be at least 6 characters",block: { (tag) in
            })
        }
        else if email.characters.count == 0{
            self.showAlertWithMsg("Please enter email",block: { (tag) in
            })
        }
        else if self.isValidEmail(candidate: email) == false{
            
            self.showAlertWithMsg("Please enter valid email",block: { (tag) in
            })
        }
        else if password.characters.count == 0{
            
            self.showAlertWithMsg("Please enter password",block: { (tag) in
            })
        }
        else{
            
            self.regiterAPICall(name, username: username, email: email, password: password)
        }
    }
    @IBAction func tapToBack(_ sender: UIButton){
        
       _ = self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

//    Body
//    {
//    "name":"{name}",
//    "username":"{username}",
//    "email":"{email}",
//    "plainPassword":"{password}"
//    }

    //MARK: - API
    func regiterAPICall(_ name: String, username: String, email: String, password: String) -> Void {
        
        KRProgressHUD.show(progressHUDStyle: .black, maskType: .black, activityIndicatorStyle: .white, font: nil, message: kLoading, image: nil, completion: nil)
        let param = [kName:name,kUsername:username,kEmail:email,kPlainPassword:password]
        NetworkManager.sharedInstance.requestApi(serviceName: kAPIRegister, method: .put, postData: param,encoding: JSONEncoding.default) {[weak self] (response, error) in
            
            defer{
                KRProgressHUD.dismiss()
            }
            guard let strongRef = self,
                let reponseMsg = response as? String else {return}
            strongRef.showAlertWithMsg(reponseMsg,block: { (tag) in
                
                if reponseMsg == "Registration successful. Please login to continue."{
                    
                    _ = strongRef.navigationController?.popViewController(animated: true)
                }
            })
        }
    }
}

//
//  NetworkUrl.swift
//  Prowareness
//
//  Created by Girijesh Kumar on 25/02/17.
//  Copyright © 2017 Girijesh Kumar. All rights reserved.
//

import Foundation

let kAppName        = "Prowareness"

/** --------------------------------------------------------
 *	API Base URL defined by Targets.
 *	--------------------------------------------------------
 */

let kBASEURL        = "http://85.222.238.74"

let kClientId    = "5836f395acf3f0f975807d11_3rnd3q1v5y4gg8s4c88w0848g0gow00c8ggsgws0w00kgoc808"
let kClientSecret    = "62erhw408yo0c4oc0480wwccc0cww44s4k4sg0c40s88gw4400"

/** --------------------------------------------------------
 * HTTP Basic Authentication
 *	--------------------------------------------------------
 */
let kContentValue   = "application/json"
let kContentType    = "Content-Type"
let kAuthentication = "Authorization"
let kAccept    = "Accept"

/** --------------------------------------------------------
 *		Used Web Services Name
 *	--------------------------------------------------------
 */
let kAPIRegister    = "/register"
let kAPILogin    = "/oauth/v2/token"
let kAPIAllWorkFlows    = "/api/v1/workflows"
let kAPICreateWorkFlow    = "/api/v1/workflow"
let kAPIGetWorkFlowDetail    = "/api/v1/workflows/%@"
let kAPIUpdateWorkFlow    = "/api/v1/workflows/%@"
let kAPIDeleteWorkFlow    = "/api/v1/workflows/%@"

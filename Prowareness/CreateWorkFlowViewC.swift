//
//  CreateWorkFlowViewC.swift
//  Prowareness
//
//  Created by Girijesh Kumar on 25/02/17.
//  Copyright © 2017 Girijesh Kumar. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class CreateWorkFlowViewC: BaseViewC {

    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var stageTxtField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var pickerHolderView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!

    var workflowDic: Any?

    let listArray:[String] = ["Hired","Not Hired","In Progress"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - Private Methods
    private func setupView(){
        
        self.setUpBoaderOnView(submitBtn)
        self.pickerHolderView.isHidden = true;
        self.title = "Create Workflow"
        self.setDataOnScreenInEditMode()
    }
    private func setDataOnScreenInEditMode(){
        
        guard let tempWorkflowDic = workflowDic as? [String:Any],
            let name = tempWorkflowDic[kName] as? String,
            let stage = tempWorkflowDic[kStage] as? String else {
            
            return;
        }
        nameTxtField.text  = name
        stageTxtField.text  = stage
        submitBtn.setTitle("update", for: .normal)
    }
    //MARK: - User Interaction
    @IBAction func tapToDone(_ sender: UIButton){
        
        
        let selectedIndex = pickerView.selectedRow(inComponent: 0)
        let stageValue = listArray[selectedIndex];
        stageTxtField.text = stageValue;
        pickerHolderView.isHidden =  true;
    }
    @IBAction func tapToSubmit(_ sender: UIButton){
        
        guard let name = nameTxtField.text,
            let stage = stageTxtField.text else {
                return;
        }
        
        if name.characters.count == 0 {
            
            self.showAlertWithMsg("Please enter name",block: { (tag) in
            })
        }
        else if stage.characters.count == 0{
            
            self.showAlertWithMsg("Please select stage",block: { (tag) in
                
            })
        }
        else{
            
            if let tempWorkflowDic = workflowDic as? [String:Any],
                let identfier = tempWorkflowDic[kIdentifier] as? String {
                    
                self.updateWorkFlowAPICall(name, stage: stage, identifer: identfier)
            }
            else{
                
                self.creatWorkFlowAPICall(name, stage: stage)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - API -
    func creatWorkFlowAPICall(_ name: String, stage: String) -> Void {
        
        KRProgressHUD.show(progressHUDStyle: .black, maskType: .black, activityIndicatorStyle: .white, font: nil, message: "loading...", image: nil, completion: nil)

        let param = [kName:name,kStage:stage]
        NetworkManager.sharedInstance.requestApi(serviceName: kAPICreateWorkFlow, method: .put, postData: param,encoding: JSONEncoding.default) { [weak self] (response, error) in
            
            defer{
                
                KRProgressHUD.dismiss()
            }
            guard let strongRef = self else {return}
            
            if let errorMsg = response as? String{
                
                strongRef.showAlertWithMsg(errorMsg,block: { (tag) in
                    
                })
            }
            if let responseData = response as? [String : Any],
                let _ = responseData[kIdentifier] as? String
            {
              _ = strongRef.navigationController?.popViewController(animated: true)
            }
        }
    }
    func updateWorkFlowAPICall(_ name: String, stage: String, identifer: String) -> Void {
        KRProgressHUD.show(progressHUDStyle: .black, maskType: .black, activityIndicatorStyle: .white, font: nil, message: kLoading, image: nil, completion: nil)

        let serviceName = String(format: kAPIUpdateWorkFlow, identifer)
        let param = [kName:name,kStage:stage]
        NetworkManager.sharedInstance.requestApi(serviceName: serviceName, method: .post, postData: param,encoding: JSONEncoding.default) { [weak self] (response, error) in
            
            defer{
                KRProgressHUD.dismiss()
            }
            guard let strongRef = self else {return}
            
            if let errorMsg = response as? String{
                
                strongRef.showAlertWithMsg(errorMsg,block: { (tag) in
                    
                })
            }
            if let responseData = response as? [String : Any],
                let _ = responseData[kIdentifier] as? String{
                
                _ = strongRef.navigationController?.popViewController(animated: true)
            }
        }
    }
}
extension CreateWorkFlowViewC: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        
        return 1;
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return listArray.count;
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        
        return listArray[row]
    }
}

extension CreateWorkFlowViewC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  textField == stageTxtField {
            self.view.endEditing(true)
            self.pickerHolderView.isHidden = false;
            return false;
        }
        self.pickerHolderView.isHidden = true;
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true;
    }
}
